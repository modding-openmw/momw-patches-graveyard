#### 01 BCoM + Mines and Caverns

Fixes an incompatibility between [Beautiful Cities of Morrowind](https://www.nexusmods.com/morrowind/mods/49231) and [Mines and Caverns](https://www.nexusmods.com/morrowind/mods/44893).

##### About

**Removed from MOMW lists in website version 6.5.0**

**Reason for removal:** No longer necessary as Mines and Caverns comes with an official BCoM compatible plugin now.

I used `tes3cmd` to delete a couple references that conflict with BCOM:

```
tes3cmd delete --exterior --id "Gnisis" 'Clean_Mines & Caverns.esp'
tes3cmd delete --interior --id "Assarnud" 'Clean_Mines & Caverns.esp'
tes3cmd delete --type pgrd --id "Assarnud" 'Clean_Mines & Caverns.esp'
```

This plugin is the result.

**Works with**:

* `Morrowind.exe/MGE-XE/MWSE`: Yes
* `Rebirth`: No

##### Load Order

Use this **instead** of the `Clean_Mines & Caverns.esp` plugin that comes with Mines & Caverns.

```
...
data="/home/username/games/openmw/Mods/CitiesTowns/BeautifulCitiesofMorrowind/00 Core"
...
data="/home/username/games/openmw/Mods/CavesAndDungeons/MinesandCaverns/Mines AND Caverns"
data="/home/username/games/openmw/Mods/Patches/MOMWPatchesGraveyard/01 BCOM+Mines and Caverns"
...
content=Beautiful cities of Morrowind.ESP
...
content=BCOM+MinesAndCaverns.esp
...
```

##### Web

* [Source on GitLab](https://gitlab.com/modding-openmw/momw-patches-graveyard/-/tree/master/01%20BCOM%2BMines%20and%20Caverns)
* [Patch Home](https://modding-openmw.gitlab.io/momw-patches-graveyard/)
