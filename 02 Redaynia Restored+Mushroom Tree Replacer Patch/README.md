#### 02 Redaynia Restored+Mushroom Tree Replacer Patch

##### About

**Removed from MOMW lists before website version 6.0.0**

**Reason for removal:** Neither Redaynia Restored or Mushroom Tree Replacer are included in any modlists anymore.

Fixes the placement of a mushroom tree for Redaynia Restored by Reizeron and Mushroom Tree Replacer by PeterBitt. Also deletes a floating sack.

**Works with**:

* `Morrowind.exe/MGE-XE/MWSE`: Probably
* `Rebirth`: No

##### Load Order

```
...
data="/home/username/games/openmw/Mods/CitiesTowns/RedayniaRestored/Data Files"
data="/home/username/games/openmw/Mods/Patches/MOMWPatchesGraveyard/02 Redaynia Restored+Mushroom Tree Replacer Patch"
...
content=Redaynia Restored.ESP
content=Redaynia Restored Mushroom Tree Replacer Patch.esp
...
```

##### Web

* [Source on GitLab](https://gitlab.com/modding-openmw/momw-patches-graveyard/-/tree/master/02%20Redaynia%20Restored+Mushroom%20Tree%20Replacer%20Patch)
* [Patch Home](https://modding-openmw.gitlab.io/momw-patches-graveyard/)
