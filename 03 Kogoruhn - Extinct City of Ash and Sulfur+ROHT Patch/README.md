#### 03 Kogoruhn - Extinct City of Ash and Sulfur+ROHT Patch

##### About

**Removed from MOMW lists before website version 6.0.0**

**Reason for removal:** Kogoruhn - Extinct City of Ash and Sulfur is not included in any modlists, plus the patch **was not updated to the latest version of Kogoruhn - Extinct City of Ash and Sulfur**!

Provides compatibility between Kogoruhn - Extinct City of Ash and Sulfur and Rise of House Telvanni by disabling various objects when a certain plot point has been reached.

**Works with**:

* `Morrowind.exe/MGE-XE/MWSE`: Probably
* `Rebirth`: No
* [`Rise of House Telvanni - 2.0`](https://www.nexusmods.com/morrowind/mods/48225): Yes

##### Load Order

```
...
data="/home/username/games/openmw/Mods/Landmasses/TamrielData"
...
data="/home/username/games/openmw/Mods/ModdingResources/OAAB_Data/00 Core"
...
data="/home/username/games/openmw/Mods/ModdingResources/Dr_Data"
...
data="/home/username/games/openmw/Mods/FloraLandscape/KogoruhnExtinctCityofAshandSulfur/Morrowind/Data Files"
data="/home/username/games/openmw/Mods/Patches/MOMWPatchesGraveyard/03 Kogoruhn - Extinct City of Ash and Sulfur+ROHT Patch"
...
content=Rise of House Telvanni.esm
content=Tamriel_Data.esm
content=OAAB_Data.esm
content=Dr_Data.esm
...
content=Riharradroon - Path to Kogoruhn v1.0.ESP
...
content=Kogoruhn - Extinct City of Ash and Sulfur.esp
content=Kogoruhn - Extinct City of Ash and Sulfur.omwscripts
content=Kogoruhn - Extinct City of Ash and Sulfur+ROHT Patch.esp
...
```

##### Web

* [Source on GitLab](https://gitlab.com/modding-openmw/momw-patches-graveyard/-/tree/master/03%20Kogoruhn%20-%20Extinct%20City%20of%20Ash%20and%20Sulfur+ROHT%20Patch)
* [Patch Home](https://modding-openmw.gitlab.io/momw-patches-graveyard/)
