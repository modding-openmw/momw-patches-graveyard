#### 04 OAAB Shipwrecks+RR Better Ships and Boats Patch

##### About

**Removed from MOMW lists before website version 6.0.0**

**Reason for removal:** RR Better Ships and Boats is not included in any modlists anymore.

Provide compatibility between [RR Mod Series - Better Ships and Boats](https://www.nexusmods.com/morrowind/mods/44001) and [OAAB Shipwrecks](https://www.nexusmods.com/morrowind/mods/51364).

**Works with**:

* `Morrowind.exe/MGE-XE/MWSE`: Probably
* `Rebirth`: No

##### Load Order

```
...
data="/home/username/games/openmw/Mods/Landmasses/TamrielData/"
...
data="/home/username/games/openmw/Mods/ModdingResources/OAAB_Data/00 Core"
...
data="/home/username/games/openmw/Mods/ObjectsClutter/RRModSeriesBetterShipsandBoats/00 - Main Files"
data="/home/username/games/openmw/Mods/ObjectsClutter/RRModSeriesBetterShipsandBoats/01 - Main ESP - English"
...
data="/home/username/games/openmw/Mods/FloraLandscape/OAABShipwrecks/00 Core"
data="/home/username/games/openmw/Mods/Patches/MOMWPatchesGraveyard/04 OAAB Shipwrecks+RR Better Ships and Boats Patch"
...
content=Tamriel_Data.esm
...
content=OAAB_Data.esm
...
content=OAAB - Shipwrecks.ESP
...
content=RR_Better_Ships_n_Boats_Eng.esp
content=OAABShipwrecks+RRBetterShipsnBoatsPatch.esp
...
```

##### Web

* [Source on GitLab](https://gitlab.com/modding-openmw/momw-patches-graveyard/-/tree/master/04%20OAAB%20Shipwrecks+RR%20Better%20Ships%20and%20Boats%20Patch)
* [Patch Home](https://modding-openmw.gitlab.io/momw-patches-graveyard/)
