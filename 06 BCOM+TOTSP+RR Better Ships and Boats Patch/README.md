#### 06 BCOM+TOTSP+RR Better Ships and Boats Patch

##### About

**Removed from MOMW lists before website version 6.0.0**

**Reason for removal:** RR Better Ships and Boats is not included in any modlists anymore, and the BCoM version of the mod is no longer officially supported and included in BCoM Patches.

Provide compatibility between [Beautiful Cities of Morrowind](https://www.nexusmods.com/morrowind/mods/49231) **pre-version 3.x**, [RR Mod Series - Better Ships and Boats](https://www.nexusmods.com/morrowind/mods/44001), and [Solstheim Tomb of the Snow Prince](https://www.nexusmods.com/morrowind/mods/46810).

The BCOM-patched plugin for RR Mod Series - Better Ships and Boats will place dupe ships to the west of TOTSP's anthology landmass location, this plugin fixes them. **NOTE** that this does not work with the standalone version of RR Mod Series - Better Ships and Boats!

**Works with**:

* `Morrowind.exe/MGE-XE/MWSE`: Probably
* `Rebirth`: No

##### Load Order

```
...
data="/home/username/games/openmw/Mods/Landmasses/TamrielData"
...
data="/home/username/games/openmw/Mods/ModdingResources/OAAB_Data/00 Core"
...
data="/home/username/games/openmw/Mods/CitiesTowns/BeautifulCitiesofMorrowind/Core/00 Core"
...
data="/home/username/games/openmw/Mods/ObjectsClutter/RRModSeriesBetterShipsandBoats/00 - Main Files"
data="/home/username/games/openmw/Mods/CitiesTowns/BeautifulCitiesofMorrowind/patches/26 RR Ships and Boats"
data="/home/username/games/openmw/Mods/Patches/MOMWPatchesGraveyard/06 BCOM+TOTSP+RR Better Ships and Boats Patch"
...
content=Tamriel_Data.esm
...
content=OAAB_Data.esm
...
content=Solstheim Tomb of The Snow Prince.esm
...
content=Beautiful cities of Morrowind.esp
...
content=RR_Better_Ships_n_Boats_Eng.esp
content=BCOM+TOTSP+RRBetterShipsnBoatsPatch.esp
...
```

##### Web

* [Source on GitLab](https://gitlab.com/modding-openmw/momw-patches-graveyard/-/tree/master/06%20BCOM+TOTSP+RR%20Better%20Ships%20and%20Boats%20Patch)
* [Patch Home](https://modding-openmw.gitlab.io/momw-patches-graveyard/)
