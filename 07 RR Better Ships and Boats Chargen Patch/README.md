#### 07 RR Better Ships and Boats Chargen Patch

##### About

**Removed from MOMW lists before website version 6.0.0**

**Reason for removal:** RR Better Ships and Boats is not included in any modlists anymore.

The guard standing outside on the deck of the boar during chargen is floating when you use [RR Mod Series - Better Ships and Boats](https://www.nexusmods.com/morrowind/mods/44001). This patch moves him back and to the side a bit so that he's level with the deck.

**Works with**:

* `Morrowind.exe/MGE-XE/MWSE`: Probably
* `Rebirth`: No

##### Load Order

```
...
content=RR_Better_Ships_n_Boats_Eng.esp
content=RRBetterShipsnBoatsChargenPatch.esp
...
```

##### Web

* [Source on GitLab](https://gitlab.com/modding-openmw/momw-patches-graveyard/-/tree/master/07%20RR%20Better%20Ships%20and%20Boats%20Chargen%20Patch)
* [Patch Home](https://modding-openmw.gitlab.io/momw-patches-graveyard/)
