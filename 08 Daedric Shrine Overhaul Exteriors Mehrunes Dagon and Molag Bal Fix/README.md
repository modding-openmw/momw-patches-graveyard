#### 08 Daedric Shrine Overhaul Exteriors Mehrunes Dagon and Molag Bal Fix

Fixes incompatibilities between [Daedric Shrine Overhaul Exteriors Mehrunes Dagon and Molag Bal](https://www.nexusmods.com/morrowind/mods/53250) and [Daedric Shrine Overhaul Molag Bal](https://www.nexusmods.com/morrowind/mods/51632).

##### About

**Removed from MOMW lists in website version 6.7.0**

**Reason for removal:** The merged version of Daedric Shrine Overhaul doesn't require the patch, and the modlists don't use the modular plugins anymore.

Removes the exterior door to the Tusenend shrine added by the exterior overhaul, since the edit is not compatible with the interior overhaul of the shrine.

**Works with**:

* `Morrowind.exe/MGE-XE/MWSE`: Yes
* `Rebirth`: No

##### Load Order

```
...
data="/home/username/games/openmw/Mods/CavesandDungeons/DaedricShrineOverhaulExteriorsMehrunesDagonandMolagBal"
...
data="/home/username/games/openmw/Mods/CavesandDungeons/DaedricShrineOverhaulMolagBal"¨
...
data="/home/username/games/openmw/Mods/Patches/MOMWPatchesGraveyard/08 Daedric Shrine Overhaul Exteriors Mehrunes Dagon and Molag Bal Fix"¨
...
content=Daedric Shrine Overhaul Ext MM.esp
...
content=Daedric Shrine Overhaul Molag Bal.esp
... 
content=DaedricShrineOverhaul_ExteriorInteriorFix.esp
...
```

##### Web

* [Source on GitLab](https://gitlab.com/modding-openmw/momw-patches-graveyard/-/tree/master/08%20Daedric%20Shrine%20Overhaul%20Exteriors%20Mehrunes%20Dagon%20and%20Molag%20Bal%20Fix)
* [Patch Home](https://modding-openmw.gitlab.io/momw-patches-graveyard/)

