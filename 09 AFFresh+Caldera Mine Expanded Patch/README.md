#### 09 AFFresh + Caldera Mine Expanded Patch

Patches incompatibilities between [AFFresh](https://www.nexusmods.com/morrowind/mods/53006) and [Caldera Mine Expanded](https://www.nexusmods.com/morrowind/mods/45194).

##### About

**Removed from MOMW lists in website version 6.7.0**

**Reason for removal:** The ESM version of AFFresh includes this fix already.

Moves two NPCs from AFFresh so they aren't stuck in static objects added by Caldera Mine Expanded.

**Works with**:

* `Morrowind.exe/MGE-XE/MWSE`: Yes
* `Rebirth`: No

##### Load Order

```
...
data="/home/username/games/openmw/Mods/CitiesTowns/CalderaMineExpanded/Caldera Mine Expanded"
...
data="/home/username/games/openmw/Mods/Quests/AFFresh"
...
data="/home/username/games/openmw/Mods/Patches/MOMWPatchesGraveyard/09 AFFresh+Caldera Mine Expanded Patch"
...
content=DD_Caldera_Expansion.esp
...
content=AFFresh.esp
...
content=AFFresh+CalderaMineExpanded.esp
...
```

##### Web

* [Source on GitLab](https://gitlab.com/modding-openmw/momw-patches-graveyard/-/tree/master/09%20AFFresh%2BCaldera%20Mine%20Expanded%20Patch)
* [Patch Home](https://modding-openmw.gitlab.io/momw-patches/)
<!-- * [Nexus Mods](https://www.nexusmods.com/morrowind/mods/#TODO) -->
