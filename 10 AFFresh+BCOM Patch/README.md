#### 10 AFFresh + BCoM Patch

Fixes incompatibilities between [Beautiful Cities of Morrowind](https://www.nexusmods.com/morrowind/mods/49231) and [AFFresh](https://www.nexusmods.com/morrowind/mods/53006).

##### About

**Removed from MOMW lists in website version 6.7.0**

**Reason for removal:** The ESM version of AFFresh includes this fix already.

Moves the placement of one NPC that ended up clipping through some architecture.

**Works with**:

* `Morrowind.exe/MGE-XE/MWSE`: Yes
* `Rebirth`: No

##### Load Order

```
...
data="/home/username/games/openmw/Mods/CitiesTowns/BeautifulCitiesofMorrowind/00 Core"
...
data="/home/username/games/openmw/Mods/Quests/AFFresh"
data="/home/username/games/openmw/Mods/Patches/MOMWPatchesGraveyard/10 AFFresh+BCOM Patch"
...
content=AFFresh.esp
...
content=Beautiful cities of Morrowind.ESP
content=AFFresh+BCOM.esp
...
```

##### Web

* [Source on GitLab](https://gitlab.com/modding-openmw/momw-patches/-/tree/master/10%20AFFresh+BCOM%20Patch)
* [Patch Home](https://modding-openmw.gitlab.io/momw-patches/)
<!-- * [Nexus Mods](https://www.nexusmods.com/morrowind/mods/#TODO) -->
