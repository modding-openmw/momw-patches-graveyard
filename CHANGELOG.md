## MOMW Patches Graveyard Changelog

#### 1.1

* Added `08 Daedric Shrine Overhaul Exteriors Mehrunes Dagon and Molag Bal Fix` by Ronik
* Added `09 AFFresh+Caldera Mine Expanded Patch` by Ronik
* Added `10 AFFresh+BCOM Patch` by johnnyhostile

[Download Link](https://gitlab.com/modding-openmw/momw-patches-graveyard/-/packages/26380882)

#### 1.0

* Initial release of the mod.

[Download Link](https://gitlab.com/modding-openmw/momw-patches-graveyard/-/packages/23496746)
