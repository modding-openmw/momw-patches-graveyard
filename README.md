# MOMW Patches Graveyard

Patches made by the Modding-OpenMW.com community that are no longer being included in any of the modlists.

**Note that all of these patches are no longer supported and come "as is"!**

#### Credits

**Authors**:

- johnnyhostile
- Ronik

**Special Thanks**:

* AFFA for making [AFFresh](https://www.nexusmods.com/morrowind/mods/53006)
* Benjamin Winger for making [Delta Plugin](https://gitlab.com/bmwinger/delta-plugin/)
* Corsair83 for making [OAAB Shipwrecks](https://www.nexusmods.com/morrowind/mods/51364)
* Demanufacturer87 for making [Kogoruhn - Extinct City of Ash and Sulfur](https://www.nexusmods.com/morrowind/mods/51615)
* Glittergear for making [Daedric Shrine Overhaul Exteriors Mehrunes Dagon and Molag Bal](https://www.nexusmods.com/morrowind/mods/53250) and [Daedric Shrine Overhaul Molag Bal](https://www.nexusmods.com/morrowind/mods/51632)
* Greatness7 for making [tes3conv](https://github.com/Greatness7/tes3conv)
* jsp25 for making [Mines and Caverns](https://www.nexusmods.com/morrowind/mods/44893)
* mort, Pozzo, Karpik777, and bhl for their work on [Rise of House Telvanni](https://www.nexusmods.com/morrowind/mods/27545) ([2.0](https://www.nexusmods.com/morrowind/mods/48225))
* RandomPal for making [Beautiful Cities of Morrowind](https://www.nexusmods.com/morrowind/mods/49231) and [Nordic Dagon Fel](https://www.nexusmods.com/morrowind/mods/49603)
* Reizeron for making [Redaynia Restored](https://www.nexusmods.com/morrowind/mods/47646)
* Resdayn Revival Team for making [RR Mod Series - Better Ships and Boats](https://www.nexusmods.com/morrowind/mods/44001)
* Team Dreamy Dremora for making [Caldera Mine Expanded](https://www.nexusmods.com/morrowind/mods/45194)
* LondonRook for making [Dr_Data](https://www.nexusmods.com/morrowind/mods/51776)
* The OpenMW team, including every contributor (for making OpenMW and OpenMW-CS)
* All the users in the `modding-openmw-dot-com` Discord channel on the OpenMW server for their dilligent testing <3
* Bethesda for making Morrowind

And a big thanks to the entire OpenMW and Morrowind modding communities! I wouldn't be doing this without all of you.

#### Web

[Project Home](https://modding-openmw.gitlab.io/momw-patches-graveyard/)

[Source on GitLab](https://gitlab.com/modding-openmw/momw-patches-graveyard)

#### Installation

1. Download the zip from a link above.
1. Extract the contents to a location of your choosing.
1. See each mod's individual `README.md` file for detailed load order information.
1. Note that these plugins won't be of any use without their dependent plugins, and these need to load after them. Enjoy!

#### Connect

* [Discord](https://discord.gg/KYKEUxFUsZ)
* [IRC](https://web.libera.chat/?channels=#momw)
* Email: `johnnyhostile at modding-openmw dot com`
