#!/bin/sh
set -e
IFS=$(echo "\n\b")
for yaml in $(find . -type f -name "*.yaml"); do
    cd "$(dirname "$yaml")"
    delta_plugin convert "$(find . -type f -name "*.yaml")"
    plugin=$(find . -type f -name "*.omwaddon")
    mv "$plugin" "$(echo "$plugin" | sed "s|omwaddon|esp|g")"
    cd ..
done

for json in $(find . -type f -name "*.json"); do
    cd "$(dirname "$json")"
    jsonFile="$(find . -type f -name "*.json")"
    tes3conv "$jsonFile" "$(echo "$jsonFile" | sed "s|json|esp|")"
    cd ..
done
