#!/bin/sh
set -eu

file_name=momw-patches-graveyard.zip

cat > version.txt <<EOF
Mod version: $(git describe --tags)
EOF

zip --must-match --recurse-paths \
    $file_name \
    "01 BCOM+Mines and Caverns" \
    "02 Redaynia Restored+Mushroom Tree Replacer Patch" \
    "03 Kogoruhn - Extinct City of Ash and Sulfur+ROHT Patch" \
    "04 OAAB Shipwrecks+RR Better Ships and Boats Patch" \
    "05 Nordic Dagon Fel+RR Better Ships and Boats Patch" \
    "06 BCOM+TOTSP+RR Better Ships and Boats Patch" \
    "07 RR Better Ships and Boats Chargen Patch" \
    "08 Daedric Shrine Overhaul Exteriors Mehrunes Dagon and Molag Bal Fix" \
    "09 AFFresh+Caldera Mine Expanded Patch" \
    "10 AFFresh+BCOM Patch" \
    CHANGELOG.md \
    LICENSE \
    README.md \
    version.txt \
    --exclude \*.json \
    --exclude \*.yaml \
    --exclude ./01\ BCOM+Mines\ and\ Caverns/scripts\* \
    --exclude ./09\ AFFresh+Caldera\ Mine\ Expanded\ Patch/scripts\*
sha256sum $file_name > $file_name.sha256sum.txt
sha512sum $file_name > $file_name.sha512sum.txt
