#!/bin/sh
set -eu
#
# This script builds the project website. It downloads soupault as needed and
# then runs it, the built website can be found at: web/build
#

this_dir=$(realpath "$(dirname "${0}")")
cd "${this_dir}"

soupault_version=4.5.0
soupault_pkg=soupault-${soupault_version}-linux-x86_64.tar.gz
soupault_path=./soupault-${soupault_version}-linux-x86_64

shas=https://github.com/PataphysicalSociety/soupault/releases/download/${soupault_version}/sha256sums
spdl=https://github.com/PataphysicalSociety/soupault/releases/download/${soupault_version}/${soupault_pkg}

if ! [ -f ${soupault_path}/soupault ]; then
    wget ${shas}
    wget ${spdl}
    tar xf ${soupault_pkg}
    grep linux sha256sums | sha256sum -c -
fi

cp ../CHANGELOG.md site/changelog.md
cp ../README.md site/readme.md

{
    cat ../01\ BCOM+Mines\ and\ Caverns/README.md
    cat ../02\ Redaynia\ Restored+Mushroom\ Tree\ Replacer\ Patch/README.md
    cat ../03\ Kogoruhn\ \-\ Extinct\ City\ of\ Ash\ and\ Sulfur+ROHT\ Patch/README.md
    cat ../04\ OAAB\ Shipwrecks+RR\ Better\ Ships\ and\ Boats\ Patch/README.md
    cat ../05\ Nordic\ Dagon\ Fel+RR\ Better\ Ships\ and\ Boats\ Patch/README.md
    cat ../06\ BCOM+TOTSP+RR\ Better\ Ships\ and\ Boats\ Patch/README.md
    cat ../07\ RR\ Better\ Ships\ and\ Boats\ Chargen\ Patch/README.md
    cat ../08\ Daedric\ Shrine\ Overhaul\ Exteriors\ Mehrunes\ Dagon\ and\ Molag\ Bal\ Fix/README.md
    cat ../09\ AFFresh+Caldera\ Mine\ Expanded\ Patch/README.md
    cat ../10\ AFFresh+BCOM\ Patch/README.md
} | grep -Ev "^\[Nexus|^\[Patch Home" >> site/readme.md

cp ../02\ Redaynia\ Restored+Mushroom\ Tree\ Replacer\ Patch/Redaynia\ Restored\ Mushroom\ Tree\ Replacer\ Patch.png site/img/
cp ../03\ Kogoruhn\ \-\ Extinct\ City\ of\ Ash\ and\ Sulfur+ROHT\ Patch/Kogoruhn\ \-\ Extinct\ City\ of\ Ash\ and\ Sulfur+ROHT\ Patch.png site/img/
cp ../06\ BCOM+TOTSP+RR\ Better\ Ships\ and\ Boats\ Patch/BCOM+TOTSP+RRBetterShipsnBoatsPatch.png site/img/
cp ../07\ RR\ Better\ Ships\ and\ Boats\ Chargen\ Patch/RRBetterShipsnBoatsChargenPatch.png site/img/

PATH=${soupault_path}:$PATH soupault "$@"
